#/bin/bash
# Create the mountpoint where the disk image will be mounted
mkdir mntpnt
# Writing a bunch of zeroes (Null bytes) into a file, which is our disk image.
dd if=/dev/zero of=devuan-686.img bs=1G count=3
# Parted requires the disk to be labelled before partitioned.
parted -s devuan-686.img mklabel msdos
# Partitioning the drisk image with a single partition and setting the boot flag as
# we want to boot from the device.
parted -s -a opt devuan-686.img mkpart primary 0% 100% set 1 boot
# We want access to the disk image as a block device. This can be achieved using a
# loop device. losetup is found in the util-linux package
losetup -P -f ./devuan-686.img /dev/loop0
# Formatting the disk image with an ext4 filesystem.
# The disk image is attached to the loop device loop0
# We can access the first (and single) partition as a block device at /dev/loop0p1
mkfs.ext4 /dev/loop0p1
# Mounting the disk image at our mountpoint directory. This will give us access to the filesystem.
mount /dev/loop0p1 mntpnt/
# Install the basic OS filesystem. We will also install some additional packages such as the kernel,
# grub bootloader and networking tools.
debootstrap --arch=i386 --variant=minbase \
--include=acpid,netbase,net-tools,ifupdown,isc-dhcp-client,linux-image-686-pae,grub-pc ascii mntpnt/ \
http://pkgmaster.devuan.org/merged
# mount proc and sys pseudo-file systems. These are directories from which there is access to
# proccess information or kernel data structures. Mounting the same sys and dev as the host to
# the disk image fs during installation. dev includes access to devices such as disks.
/bin/mount -t proc proc mntpnt/proc
/bin/mount -o bind /dev mntpnt/dev
/bin/mount -o bind /sys mntpnt/sys
# Running these commands in the chroot environment, from bin/bash until the EOF line
cat << EOF | chroot mntpnt/ /bin/bash
# Setting hostname
echo myimage > /etc/hostname
echo 'Acquire::http { Proxy "http://apt.example.com:3142"; };' >> /etc/apt/apt.conf.d/01proxy
echo deb http://pkgmaster.devuan.org/merged ascii-backports main >> /etc/apt/sources.list
# Install the Grub boot loader.
grub-install /dev/loop0
update-grub2
# Configure network configuration for eth0 nic. Will use dhcp here.
printf "auto eth0\niface eth0 inet dhcp" >> /etc/network/interfaces
# Set a default password
echo "root:secret_password_here" | chpasswd
# Exit the chroot environment
EOF
Appendix 1
2 (1)
# Cleanup. Unmount devices. Disconnect loop device and remove mountpoint directory.
/bin/umount mntpnt/proc
/bin/umount mntpnt/dev
/bin/umount mntpnt/sys
/bin/umount mntpnt/
losetup -d /dev/loop0
rmdir mntpnt
# Image should now be usable, file devuan-686.img
