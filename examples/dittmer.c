/* HTTP Server */
/* Based on the simple http_server example, included in the espressif IDF */
/* which is in the Public Domain (or optionally CC0 licenced), */
#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <string.h>
#include "driver/gpio.h"
#include <esp_http_server.h>
// pueba
// commit PANCHO
// holis
#define BLINK_GPIO 2
#define EXAMPLE_WIFI_SSID "WiFISSID"
#define EXAMPLE_WIFI_PASS "WiFiPassword"

static const char *TAG = "APP";
char led_status[10] = "OFF";
void set_led_state();

// Blink the LED in a seperate task
void blink_task(void *pvParameter)
{
    while (strcmp("BLINK", led_status) == 0)
    {
        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
    set_led_state();
    vTaskDelete(NULL);
}

// Set LED in correct state
void set_led_state()
{
    if (strcmp("OFF", led_status) == 0)
    {
        gpio_set_level(BLINK_GPIO, 0);
    }
    if (strcmp("ON", led_status) == 0)
    {
        gpio_set_level(BLINK_GPIO, 1);
    }
    if (strcmp("BLINK", led_status) == 0)
    {
        xTaskCreate(&blink_task, "blink_task", configMINIMAL_STACK_SIZE, NULL, 5,
                    NULL);
    }
}

/* An HTTP GET handler */
esp_err_t hello_get_handler(httpd_req_t *req)
{
    char *buf;
    size_t buf_len;
    /* Get header value string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
    if (buf_len > 1)
    {
        buf = malloc(buf_len);
        /* Copy null terminated value string into buffer */
        if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK)
        {
            ESP_LOGI(TAG, "Found header => Host: %s", buf);
        }
        free(buf);
    }
    Appendix 2 2(1)
        /* Send response with custom headers and body set as the
         * string passed in user context*/
        const char *resp_str = (const char *)req->user_ctx;
    httpd_resp_send(req, resp_str, strlen(resp_str));
    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    if (httpd_req_get_hdr_value_len(req, "Host") == 0)
    {
        ESP_LOGI(TAG, "Request headers lost");
    }
    return ESP_OK;
}

httpd_uri_t hello = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = hello_get_handler,
    .user_ctx = "<html><body><h1>Hello there!</h1>This is the esp32</body></html>"};

char *parse_post(char message_str[100])
{
    // Parse status request
    if (strncmp("STATUS", message_str, 6) == 0)
    {
        ESP_LOGI(TAG, "==================STATUS==============");
        if (strcmp("LEDS", message_str + 7) == 0)
        {
            ESP_LOGI(TAG, "==================LEDS==============");
            if (strcmp("OFF", led_status) == 0)
            {
                return "OFF";
            }
            if (strcmp("ON", led_status) == 0)
            {
                return "ON";
            }
            if (strcmp("BLINK", led_status) == 0)
            {
                return "BLINK";
            }
        }
        if (strcmp("PING", message_str + 7) == 0)
        {
            return "PONG";
        }
    }
    // Parse SETLED request
    if (strncmp("SETLED", message_str, 6) == 0)
    {
        ESP_LOGI(TAG, "==================Setting LED==============");
        if (strcmp("ON", message_str + 7) == 0)
        {
            ESP_LOGI(TAG, "==================LED ON==============");
            strcpy(led_status, "ON");
            return led_status;
        }
        if (strcmp("OFF", message_str + 7) == 0)
        {
            ESP_LOGI(TAG, "==================LED OFF==============");
            strcpy(led_status, "OFF");
            return led_status;
        }
        if (strcmp("BLINK", message_str + 7) == 0)
        {
            ESP_LOGI(TAG, "==================LED BLINK==============");
            strcpy(led_status, "BLINK");
            return led_status;
        }
    }
    return "Can't Parse Message";
}

Appendix 2 3(1)
    /* An HTTP POST handler */
    esp_err_t led_post_handler(httpd_req_t *req)
{
    char buf[100];
    int ret, remaining = req->content_len;
    while (remaining > 0)
    {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                                  MIN(remaining, sizeof(buf)))) <= 0)
        {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT)
            {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        /* Log data received */
        ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
        ESP_LOGI(TAG, "%.*s", ret, buf);
        ESP_LOGI(TAG, "====================================");
        char *return_message = parse_post(buf);
        set_led_state(led_status);
        ESP_LOGI(TAG, "Sending now: %s : %i", return_message, strlen(return_message));
        httpd_resp_send_chunk(req, return_message, strlen(return_message));
        remaining -= ret;
    }
    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

httpd_uri_t led = {
    .uri = "/led",
    .method = HTTP_POST,
    .handler = led_post_handler,
    .user_ctx = NULL};

httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &hello);
        httpd_register_uri_handler(server, &led);
        return server;
    }
    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

Appendix 2 4(1) void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    httpd_handle_t *server = (httpd_handle_t *)ctx;
    switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        ESP_ERROR_CHECK(esp_wifi_connect());
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "Got IP: '%s'",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        /* Start the web server */
        if (*server == NULL)
        {
            *server = start_webserver();
        }
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        ESP_ERROR_CHECK(esp_wifi_connect());
        /* Stop the web server */
        if (*server)
        {
            stop_webserver(*server);
            *server = NULL;
        }
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void initialise_wifi(void *arg)
{
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, arg));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_WIFI_SSID,
            .password = EXAMPLE_WIFI_PASS,
        },
    };
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}

Appendix 2 5(1) void app_main()
{
    gpio_pad_select_gpio(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(BLINK_GPIO, 0);
    static httpd_handle_t server = NULL;
    ESP_ERROR_CHECK(nvs_flash_init());
    initialise_wifi(&server);
}
