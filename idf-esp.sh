#!/bin/bash
sudo apt install git wget flex bison gperf python3 python3-pip python3-setuptools cmake ninja-build ccache libffi-dev libssl-dev dfu-util libusb-1.0-0
mkdir -p /idf-esp
cd /idf-esp
git clone https://github.com/espressif/esp-idf.git
cd ./esp/esp-idf
./install.sh esp32
sudo chmod +x ./export.sh
. ./export.sh
idf.py create-project blink-led
cd ./blink-led
idf.py build
