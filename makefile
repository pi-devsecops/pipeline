CFLAGS = -Wall -pedantic -Wextra -Wconversion -std=gnu11 -O2 -Werror
CC = gcc

all: folders main tests
install:
#sudo apt install netpbm -y

folders:
	mkdir -p ./bin ./obj

main: welcome.o ./src/main.c
	$(CC) $(CFLAGS) ./src/main.c ./bin/welcome.o -o ./bin/main 

tests: welcome.o ./tests/test_main.c
	$(CC) $(CFLAGS) ./tests/test_main.c ./bin/welcome.o -o ./bin/test_main 

welcome.o: ./src/welcome.c
	$(CC) $(CFLAGS) -c ./src/welcome.c -o ./obj/welcome.o

clean:
	rm -rf ./bin *.o
