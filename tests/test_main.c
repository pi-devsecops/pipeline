#include <assert.h>
#include <string.h>

#include "../src/welcome.h"

int main()
{
  assert(0 == strcmp("Welcome to DevOps\n", str_welcome()));
  return (0);
}